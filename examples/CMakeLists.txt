set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=gnu11 -Wall  -pthread")

add_executable(example_logging_threads_cond logging_threads_cond.c)
add_executable(example_logging_threads logging_threads.c)

