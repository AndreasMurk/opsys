//Few compiling options: Autocompile and Run with cmake project 'GritschPhilippTest' | consider required cmake version for that
//if that doesn't work gcc -Wall -Werror -std=gnu11 -pthread gritsch_source.c -o GritschPhilippTest && ./GritschPhilippTest

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include <sys/types.h>
#include <sys/fcntl.h>
#include <syscall.h>
#include <limits.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

#include <semaphore.h>
#include <sys/stat.h>

#define THREAD_NO 4
#define NAMESIZE 32
#define MESSAGE_FIFO "MESS_FIFO"
#define TICKET_FIFO "TICK_FIFO"

#define ITERATIONS 5

#define BUF_SIZE 64

#define STD_PERMISSIONS 0644

//following 2 functions are from my solution exercise 7, task1 common.h
void error_handler(char *msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

void error_handler_thread(char *msg) {
    perror(msg);
    pthread_exit(NULL);
}

///////////////////////////////////////////////////////////////////////////////


void *sender_thread(void *arg) {
    char identifier[NAMESIZE];
    strcpy(identifier, (char *) arg);

    int ticket_fifo_fd, message_fifo_fd;
    char ticket[BUF_SIZE];
    char message[BUF_SIZE];

    if ((ticket_fifo_fd = open(TICKET_FIFO, O_RDONLY)) == -1) {
        error_handler_thread("open ticket for reading");
    }

    if ((message_fifo_fd = open(MESSAGE_FIFO, O_WRONLY)) == -1) {
        error_handler_thread("open message ticket for writing");
    }

    for (int i = 0; i < ITERATIONS; i++) {
        if (read(ticket_fifo_fd, ticket, BUF_SIZE) > 0) {
            sprintf(message, "%s %s", ticket, identifier);
            write(message_fifo_fd, message, BUF_SIZE);
        }
    }

    close(message_fifo_fd);
    close(ticket_fifo_fd);
    pthread_exit(NULL);
}

void *logger_thread(void *arg) {
    int ticket_fifo_fd, message_fifo_fd;
    char ticket[BUF_SIZE];
    char message[BUF_SIZE];

    if ((ticket_fifo_fd = open(TICKET_FIFO, O_WRONLY) == -1) {
        error_handler_thread("open ticket for writing");
    }

    if ((message_fifo_fd = open(MESSAGE_FIFO, O_RDONLY)) == -1) {
        error_handler_thread("open message for reading");
    }

    for (int i = 1; i <= 3 * ITERATIONS; i++) {
        sprintf(ticket, "%d", i);
        write(ticket_fifo_fd, ticket, BUF_SIZE);

        if (read(message_fifo_fd, message, BUF_SIZE) > 0) {
            fprintf(stdout, "Received: %s\n", message);
        }
    }
    close(message_fifo_fd);
    close(ticket_fifo_fd);
    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    pthread_t sender_thread_id_vector[THREAD_NO - 1], logger_thread_id;
    char thread_argument[THREAD_NO - 1][NAMESIZE];
    strcpy(thread_argument[0], "web_server");
    strcpy(thread_argument[1], "database");
    strcpy(thread_argument[2], "weather_station");

    mkfifo(TICKET_FIFO, STD_PERMISSIONS);
    mkfifo(MESSAGE_FIFO, STD_PERMISSIONS);

    for (int i = 0; i < THREAD_NO - 1; i++) {
        if (pthread_create(&sender_thread_id_vector[i], NULL, sender_thread, (void *) thread_argument[i]) != 0) {
            error_handler("pthread_create");
        }
    }

    if (pthread_create(&logger_thread_id, NULL, logger_thread, NULL) != 0) {
        error_handler("pthread_create");
    }

    for (int i = 0; i < THREAD_NO - 1; i++) {
        if (pthread_join(sender_thread_id_vector[i], NULL) != 0) {
            error_handler("pthread_join");
        }
    }

    if (pthread_join(logger_thread_id, NULL) != 0) {
        error_handler("pthread_join");
    }

    unlink(TICKET_FIFO);
    unlink(MESSAGE_FIFO);

    return EXIT_SUCCESS;
}
