#define _GNU_SOURCE
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <time.h>
#include <string.h>
#include <sys/select.h>
#include <limits.h>
#include <sys/fcntl.h>
#include <signal.h>

const int MESSAGE_SIZE = 20;

#define MESSAGE_QUEUE "WEB_FIFO"
#define DATA_MQ "DATA_FIFO"
#define MIDDLE_MQ "MIDDLE_FIFO"

void err_sys(const char *x){ //function to close process
    perror(x);
    fcloseall();
    exit(EXIT_FAILURE);
}
//signal handler, if wheter 1 child terminates, or a parent is aborted, sigint is sent to all other processes, which then terminate itself
void signal_handler(int signal){
    switch(signal){
        case SIGTERM: 
        case SIGCHLD:
        case SIGINT:
            //On process stop unlink all fifos, kill all children and parent
            unlink(MESSAGE_QUEUE);
            unlink(DATA_MQ);
            unlink(MIDDLE_MQ);
            kill(0,SIGINT);
            err_sys("one child or parent terminated unexpectedly");
            exit(EXIT_FAILURE);
    }
}


void create_server_process(char *fifoname){
    switch(fork()){
        case -1:
            err_sys("fork");
            break;
        case 0:;
            int fd = open(fifoname, O_WRONLY);
            if(fd==-1) err_sys("open w");

            while(1){
                sleep(rand()%6+2);
                write(fd,fifoname,MESSAGE_SIZE);
            }
    }
}


int main(int argc, char** argv){

    //signal handling
    struct sigaction action;
    action.sa_handler = signal_handler;
    action.sa_flags = SA_NOCLDWAIT;
    sigemptyset(&action.sa_mask);

    sigaction(SIGINT,&action,NULL);
    sigaction(SIGCHLD,&action,NULL);
    sigaction(SIGTERM,&action,NULL);

    //random number generator init
    srand(time(NULL));

    //defining max message size
    if(MESSAGE_SIZE>PIPE_BUF) err_sys("MESSAGE_SIZE");

    //messages
    char message_webserver[] = "web";
    char message_middlewareserver[] = "middle-ware";
    char message_databaseserver[] = "database";


    //handling if message size is to large
    if (strlen(message_databaseserver) > MESSAGE_SIZE) err_sys("MESSAGE_SIZE1");
    if (strlen(message_middlewareserver) > MESSAGE_SIZE) err_sys("MESSAGE_SIZE2");
    if (strlen(message_webserver) > MESSAGE_SIZE) err_sys("MESSAGE_SIZE3");

    //Moved fifo creation here, as it occured, that fifos haven't existed when opening them in the consumer process
    if (mkfifo(MESSAGE_QUEUE, 0777) != 0) err_sys("mkfifo");
    if (mkfifo(DATA_MQ, 0777) != 0) err_sys("mkfifo");
    if (mkfifo(MIDDLE_MQ, 0777) != 0) err_sys("mkfifo");

    //create three processes
    create_server_process(MESSAGE_QUEUE);
    create_server_process(DATA_MQ);
    create_server_process(MIDDLE_MQ);

    //last process
    switch(fork()){
        case -1:
            err_sys("fork");
        case 0:;
            int fd_webserver = open(MESSAGE_QUEUE, O_NONBLOCK | O_RDONLY);
            int fd_middlewareserver = open(MIDDLE_MQ, O_NONBLOCK | O_RDONLY);
            int fd_database = open(DATA_MQ, O_NONBLOCK | O_RDONLY);

            if(fd_middlewareserver < 0 || fd_webserver < 0 || fd_database < 0) err_sys("open r");

            fd_set fdset;
            int maxfd = fd_webserver>fd_middlewareserver?fd_webserver:fd_middlewareserver;
            maxfd = maxfd>fd_database?maxfd:fd_database;

            char buf[MESSAGE_SIZE];

            int counter_web = 0;
            int counter_middle =0;
            int counter_data = 0;
            while(1){
                FD_ZERO(&fdset);
                FD_SET(fd_webserver,&fdset);
                FD_SET(fd_middlewareserver,&fdset);
                FD_SET(fd_database,&fdset);
                
                if(select(maxfd+1, &fdset, NULL, NULL, NULL)<0) err_sys("select");

                if(FD_ISSET(fd_webserver,&fdset)){
                    read(fd_webserver,buf,MESSAGE_SIZE);
                    printf("[%s] message %i received from Web-Server\n",buf,++counter_web);
                } else if(FD_ISSET(fd_middlewareserver,&fdset)){
                    read(fd_middlewareserver,buf,MESSAGE_SIZE);
                    printf("[%s] received %i from Middleware-Server\n",buf,++counter_middle);
                } else if(FD_ISSET(fd_database,&fdset)){
                    read(fd_database,buf,MESSAGE_SIZE);
                    printf("[%s] received %i from Database\n",buf,++counter_data);
                }
            }

    }
    pause();
    fcloseall();


    return EXIT_SUCCESS;

}
