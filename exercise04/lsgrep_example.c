#define _GNU_SOURCE
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <wait.h>

void err_sys(const char *x){
    perror(x);
    fcloseall();
    exit(EXIT_FAILURE);
}

int main(int argc, char** argv){
    int fd[2];
    char *const exec_ls[] = {"ls",NULL};
    char *const exec_grep[] = {"grep",argv[1],NULL};
    
    if(pipe(fd)<0)
        err_sys("pipe");
    
    switch(fork()){
    case -1:
        err_sys("fork1");
        break;
    case 0:
        //child: closes its stdout and sets fd[1] as OUT
        dup2(fd[1],STDOUT_FILENO);
            //closed fd[0]
        close(fd[0]);

        if(execvp(exec_ls[0],exec_ls)<0)
            err_sys("exec ls failed");
        exit(EXIT_SUCCESS);
    }

    //main process continues here, and forks again
    switch(fork()){
    case -1:
        err_sys("fork2");
        break;
    case 0:
        //child2 closes stdin, and copies fd[0] as IN
        dup2(fd[0],STDIN_FILENO);
            //child2 doesn't send anyghing
        close(fd[1]);
        if(execvp(exec_grep[0],exec_grep)<0)
            err_sys("exec grep failed");
        exit(EXIT_SUCCESS);
    }
    //parent has to close both ends, and waits for both children
    close(fd[0]);
    close(fd[1]);
    wait(0);
    wait(0);

    return EXIT_SUCCESS;

}
