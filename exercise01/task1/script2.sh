#!/bin/bash


#function usage: 
#input: -
#output: prints the given string on the screen and exits with status failure
function usage {
    echo "$0: <TODO: fill me in>"
    exit 1
}



#3 arguments are necessary, if one is missing, call usage
#shift = shift 1, shifts parameter list to the left and deletes the leftest argument
ARG1=$1; shift || usage #save first argument FILE in which grep should search
ARG2=$1; shift || usage #save second argument FILE or stream to write the result
ARG3=$1; shift || usage #save third argument PATTERN

grep -n "$ARG3" "$ARG1" > "$ARG2" #grep [OPTIONS] PATTERN [FILE] > [FILE] (the greater sign redirects stdout to $ARG2)

# -n includes linenumbers
#therefore script2.sh <input> <output> <pattern> input file, file where the results of the search should be written, and the pattern for the search