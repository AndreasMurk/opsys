#!/bin/bash
for FN in "$@" #for String in parameter list
do
    chmod 0750 "$FN" #change rights to RWXR-X---
done