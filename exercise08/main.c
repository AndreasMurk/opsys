#include <limits.h>
#include <stdio.h>
#include <math.h>

#include "scheduling_sim.h"
#include "scheduling_utility.h"

// ---------------------------------------------------------------------------
// Example schedulers
// ---------------------------------------------------------------------------

process_t* fcfs(const int __attribute__((unused)) timestep, processes_t* processes) {
	process_t* selected = NULL;
	int min_arrival_time = INT_MAX;

	for(int i = 0; i < processes->num_processes; ++i) {
		process_t* proc = processes->procs[i];
		if(proc->yielded) {
			proc->user1 = 0;
			continue;
		}

		if(proc->user1 == 1) { return proc; }

		if(proc->arrival_time < min_arrival_time) {
			min_arrival_time = proc->arrival_time;
			selected = proc;
		}
	}

	if(selected == NULL) { selected = processes->procs[0]; }
	selected->user1 = 1;
	return selected;
}

process_t* round_robin(const int __attribute__((unused)) timestep, processes_t* processes_struct) {
	process_t** processes = processes_struct->procs;
	int num_processes = processes_struct->num_processes;

	process_t* selected = processes[0];
	int next_idx = 1 % num_processes;
	for(int i = 0; i < num_processes; ++i) {
		if(processes[i]->user1 == 1) {
			selected = processes[i];
			next_idx = (i + 1) % num_processes;
			break;
		}
	}

	selected->user1 = 0;
	process_t* next = processes[next_idx];
	if(next != selected) { next->user1 = 1; }
	return selected;
}

// ---------------------------------------------------------------------------
// Implement your schedulers here
// ---------------------------------------------------------------------------
//Task 1
process_t* srt(const int __attribute__((unused)) timestep, processes_t *processes_struct){
    process_t* selected = NULL;
    process_t* selected_yield = NULL;
    int num_processes = processes_struct->num_processes;
    int shortest_remaining_time = INT_MAX;
    int shortest_remaining_time_yielded = INT_MAX;

    //find process with the shortest remaining time
    for(int i = 0; i<num_processes; i++) {
        process_t *currentproc = processes_struct->procs[i];
        //find the shortest process which hasn't yielded
        if (!currentproc->yielded) {
            if (currentproc->remaining_time < shortest_remaining_time) {
                shortest_remaining_time = currentproc->remaining_time;

                selected = currentproc;
            }
        } //find the shortest process which has yielded
        else {
            if (currentproc->remaining_time < shortest_remaining_time_yielded) {
                shortest_remaining_time_yielded = currentproc->remaining_time;

                selected_yield = currentproc;
            }
        }

    }

    //if there is no process which hasn't yielded, choose the process (which yielded) and has the shortest remaining time
    if(selected == NULL){
        return selected_yield;
    }

    return selected;
}

process_t* round_robin_q3(const int __attribute__((unused)) timestep, processes_t *processes_struct){
    static int QUANTUM = 3;
    int num_processes = processes_struct->num_processes;
    process_t* selected = NULL;
    process_t* selected_yield = NULL;

    //user 1 stores the current quantum
    //user 2 stores the information, when a process can be rescheduled
    //user 3 is 0 if user2 was never set, and 1 if user2 was set at least once

    //when there is already a process for the current quantum
    for(int i = 0; i<num_processes; i++){
        process_t* currentproc = processes_struct->procs[i];

        //if user2 has never been modified, initialize user2 with the arrival time
        if(currentproc->user3==0){
            currentproc->user2 = currentproc->arrival_time;
            currentproc->user3 = 1;
        }

        //if there is a process with an active quantum
        if(currentproc->user1){
            //and the process qquantum is smaller than the allowed quantum
            if(currentproc->user1<QUANTUM && !currentproc->yielded){
                selected = currentproc;

            } else {
                //when it yielded or quantum is reached
                currentproc->user1=0;
                currentproc->user2=timestep;
                currentproc->user3=1;
            }
        }
    }

    //when there was no process which already was running, or yielded, we choose a new one
    if(selected==NULL){
        int smallest_arrival_time= INT_MAX;
        int smallest_arrival_time_yielded = INT_MAX;

        //find process with the smallest user2
        for(int i = 0; i<num_processes; i++) {
            process_t *currentproc = processes_struct->procs[i];
            //find the smallest process which hasn't yielded
            if (!currentproc->yielded) {
                if (currentproc->user2 < smallest_arrival_time) {
                    smallest_arrival_time = currentproc->user2;
                    selected = currentproc;
                }
            } //find the smallest process which has yielded
            else {
                if (currentproc->user2 < smallest_arrival_time_yielded) {
                    smallest_arrival_time_yielded = currentproc->user2;
                    selected_yield = currentproc;
                }
            }
        }

        //if there is no process which hasn't yielded, choose the process (which yielded) and has the smallest user2
        if(selected == NULL){
            selected = selected_yield;
        }
    }

    selected->user1++;
    return selected;
}


//Task2
process_t* hpf_p(const int __attribute__((unused)) timestep, processes_t *processes_struct){
    process_t* selected = NULL;
    process_t* selected_yield = NULL;
    int num_processes = processes_struct->num_processes;
    int highest_priority = 0;
    int highest_priority_yielded = 0;

    //find process with the hightest priority
    for(int i = 0; i<num_processes; i++) {
        process_t *currentproc = processes_struct->procs[i];
        //find the process with the hightest priority which hasn't yielded
        if (!currentproc->yielded) {
            if (currentproc->priority > highest_priority) {
                highest_priority = currentproc->priority;
                selected = currentproc;
            }
        } //find the process with the hightest priority which has yielded
        else {
            if (currentproc->priority > highest_priority_yielded) {
                highest_priority_yielded = currentproc->priority;
                selected_yield = currentproc;
            }
        }

    }

    //if there is no process which hasn't yielded, choose the process (which yielded) and has the shortest remaining time
    if(selected == NULL){
        return selected_yield;
    }

    return selected;
}

process_t* hpf_np(const int __attribute__((unused)) timestep, processes_t *processes_struct){
    process_t* selected = NULL;
    int num_processes = processes_struct->num_processes;
    //user1 stores wheter a process is already running

    //when there is already a process running
    for(int i = 0; i<num_processes; i++){
        process_t* currentproc = processes_struct->procs[i];

        //if there is an active process
        if(currentproc->user1){
            //and the process didnt yield
            if(!currentproc->yielded){
                //the next process is the current process and adjust quantum
                selected = currentproc;
            } else {
                //when it yielded, it looses its execution status
                currentproc->user1=0;
            }
        }
    }

    //if there is no active process which hasn't yielded
    if(selected == NULL){
        //find the process with the highest priority
        selected = hpf_p(timestep,processes_struct);
        selected->user1=1;
    }

    return selected;
}

process_t* lottery(const int __attribute__((unused)) timestep, processes_t *processes_struct){
    process_t* selected = NULL;
    const int QUANTUM = 3;
    const int TICKET_DISTR = 30;
    int num_processes = processes_struct->num_processes;

    //user1 stores the quantum of the current process
    //user2 stores the tickets of the processes
    //user3 stores if the tickets are valid, or if they have to be recalculated

    //when there is already a process running
    for(int i = 0; i<num_processes; i++){
        process_t* currentproc = processes_struct->procs[i];

        float tickets = (float) TICKET_DISTR/currentproc->service_time;

        if(currentproc->user3==0) {
            if (currentproc->yielded) {
                currentproc->user2 = roundf(tickets * QUANTUM/(currentproc->user1));
                currentproc->user3 = 1;
            } else {
                currentproc->user2 = roundf(tickets);
                currentproc->user3 = 0;
            }
        }

        if(currentproc->user1){
            if(currentproc->user1<QUANTUM && !currentproc->yielded){
                selected = currentproc;
            } else {
                currentproc->user1 = 0;
            }
        }
    }

    //if there is no active process which hasn't yielded, we need  to choose
    if(selected == NULL){
        int ticket_sum = 0;

        for(int i = 0; i<num_processes; i++){
            process_t* currentproc = processes_struct->procs[i];
            ticket_sum+=currentproc->user2;
            printf("%c with %d\n",currentproc->name,currentproc->user2);
        }

        int result = scheduler_rand()%(ticket_sum) + 1;
        fprintf(stdout, "%d: %d --- total was %d\n", timestep,result,ticket_sum);
        int i = 0;
        int cumulated_propability = 0;
        do{
            selected = processes_struct->procs[i++];
            cumulated_propability+=selected->user2;
        } while(result>=cumulated_propability);
    }

    selected->user1++;
    selected->user3=0;
    return selected;
}




// ---------------------------------------------------------------------------

int main(int argc, char** argv) {
	if(argc < 2) {
		fprintf(stderr, "Error: Usage: %s <filename>\nExample: %s input.csv\n", argv[0], argv[0]);
		return EXIT_FAILURE;
	}

	processes_t* procs = read_simulation_data(argv[1]);
	print_simulation_data(stdout, procs);

	// -----------------------------------------------------------------------
	// Insert calls to your schedulers here
	// -----------------------------------------------------------------------

	print_schedule(stdout, compute_schedule(procs, fcfs), procs);
	print_schedule(stdout, compute_schedule(procs, round_robin), procs);

    print_schedule(stdout, compute_schedule(procs, srt), procs);
    print_schedule(stdout, compute_schedule(procs, round_robin_q3), procs);

    print_schedule(stdout, compute_schedule(procs, hpf_p), procs);
    print_schedule(stdout, compute_schedule(procs, hpf_np), procs);

    print_schedule(stdout, compute_schedule(procs, lottery), procs);



    // -----------------------------------------------------------------------

	free_processes(procs);

	return EXIT_SUCCESS;
}
