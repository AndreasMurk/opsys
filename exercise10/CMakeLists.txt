cmake_minimum_required(VERSION 3.14)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=gnu11 -Wall -Werror ")
add_executable(ex10_membench membench.c common.h)
add_executable(ex10_membench_bestfit_global own_bestfit_global.h membench_bestfit_global.c common.h)
add_executable(ex10_membench_bestfit_local own_bestfit_local.h membench_bestfit_local.c common.h)
add_executable(ex10_membench_freelist_global own_freelist_global.h membench_freelist_global.c common.h)
add_executable(ex10_membench_freelist_local own_freelist_local.h membench_freelist_local.c common.h)

target_link_libraries(ex10_membench pthreads)
target_link_libraries(ex10_membench_bestfit_global pthreads)
target_link_libraries(ex10_membench_bestfit_local pthreads)
target_link_libraries(ex10_membench_freelist_global m pthreads)
target_link_libraries(ex10_membench_freelist_local m pthreads)

