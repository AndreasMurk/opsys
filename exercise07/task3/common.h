#define _GNU_SOURCE
#define _POSIX_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <ctype.h>
#include <pthread.h>

#define THREAD_NO 1000
#define ITERATIONS 10000

void error_handler(char* msg){
    fcloseall();
    perror(msg);
    exit(EXIT_FAILURE);
}

void error_handler_thread(char* msg){
    perror(msg);
    pthread_exit(NULL);
}