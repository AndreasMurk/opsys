
#include "common.h"

void* counter_thread(void *param){

    int* global_value = (int*) param;

    for(int i = 0; i<ITERATIONS;i++){
        (*global_value)++;
    }

    pthread_exit(NULL);

}


int main(void){

    int glob_value = 0;

    pthread_t thread_vector[THREAD_NO];

    //create THREAD_NO threads
    for(int i = 0; i<THREAD_NO; i++){

        if( pthread_create(&thread_vector[i],NULL,counter_thread,(void*) &glob_value) != 0 )
            error_handler("pthread_create");

    }

    //wait for all threads
    for(int i = 0; i<THREAD_NO;i++){
        if( pthread_join(thread_vector[i],NULL) != 0 )
            error_handler("pthread_join");

    }

    //print value to stdout
    //fprintf(stdout,"Value calculated was %d\n",glob_value);


    return EXIT_SUCCESS;

}
