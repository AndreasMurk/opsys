
#include "common.h"
#include <stdatomic.h>


void* counter_thread(void *param){

    atomic_int* global_value = (atomic_int*) param;

    for(int i = 0; i<ITERATIONS;i++){
        atomic_fetch_add(global_value,1);
    }

    pthread_exit(NULL);

}


int main(void){

    atomic_int glob_value = 0;

    pthread_t thread_vector[THREAD_NO];

    //create THREAD_NO threads
    for(int i = 0; i<THREAD_NO; i++){

        if( pthread_create(&thread_vector[i],NULL,counter_thread,(void*) &glob_value) != 0 )
            error_handler("pthread_create");

    }

    //wait for all threads
    for(int i = 0; i<THREAD_NO;i++){
        if( pthread_join(thread_vector[i],NULL) != 0 )
            error_handler("pthread_join");

    }

    //print value to stdout
   //fprintf(stdout,"Value calculated was %d\n",glob_value);


    return EXIT_SUCCESS;

}
