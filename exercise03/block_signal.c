#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>

pid_t child; 
sigset_t block_mask;



void child_handler(int signal){
    switch(signal){
        case SIGUSR1: //when sigusr1 is received print
            printf("Message to the stdout\n");
            break;
        case SIGUSR2: //when sigusr2 is received exit
            exit(0);
        case SIGALRM:
            printf("Child received alarm, sigusr2 unblock\n");  //when sigalarm is received, unblock the blocked mask (global variable)
            sigprocmask(SIG_UNBLOCK,&block_mask,NULL);
    }
}

void parent_handler(int signal){
    switch(signal){ 
        case SIGALRM:
            kill(child,SIGUSR1);  //when sigalarm is received by parent, send signal sigusr 1 to child
            printf("Parent sent sigusr1\n");
            return;
        case SIGCHLD:  
            exit(0); //when child terminates, also terminate parent
    }
}


int main(void){
    pid_t current_pid = fork();

    switch(current_pid){
        case -1:
            perror("fork");
            return EXIT_FAILURE;
        case 0:
            ;  //empty statement for compiler
            sigset_t unblocked_mask;  //Mask for unblock processes
            sigemptyset(&block_mask);
            sigaddset(&block_mask,SIGUSR2);  //add sigusr2 to the blockmask
            sigemptyset(&unblocked_mask);    //clear the unblock mask

            struct sigaction child_action;  //sigaction struct for handling
            child_action.sa_mask = unblocked_mask;   //mask should be unblocked
            child_action.sa_handler = child_handler;
            child_action.sa_flags = 0;

            sigaction(SIGUSR1,&child_action,NULL); //signals sigusr1 sigusr2 and sigalarm are given to child_action
            sigaction(SIGUSR2,&child_action,NULL);
            sigaction(SIGALRM,&child_action,NULL);

            sigprocmask(SIG_BLOCK,&block_mask,NULL); //block sigusr2

            alarm(13);  //alarm 13

            while(1){  //wait for incoming alarms
                pause();
            }
            
        default:

            sleep(5);   //wait
            child = current_pid;  //set global var, for handler
            kill(child,SIGUSR2);   //send sigusr2 to child

            struct sigaction parent_action;  //struct for signalhandling parent
            parent_action.sa_handler = parent_handler;
            sigemptyset(&parent_action.sa_mask);
            parent_action.sa_flags = 0;

            sigaction(SIGALRM,&parent_action,NULL); //adding struct to signals
            sigaction(SIGCHLD,&parent_action,NULL);

            while(1){
                alarm(5);  //alarm all 5 seconds
                pause();  //wait for signals
            }
    }
}