#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define PROC_NO 7

int main(void){

    for (int i = 0; i < PROC_NO; i++) {
        //creating 7 child processes
        switch (fork()) {
            //when we are child, print short messsage, and exit
			case 0:
                printf("child %d\n", i);
				exit(0);
			case -1:
				perror("fork");
				return EXIT_FAILURE;
		}
	}

    for (int i = 0; i < PROC_NO; i++) {
        wait(0);
    }

	return EXIT_SUCCESS;
}
