#include "common_sysv.h"

void err_sys(const char *x){ //function to close process
    fcloseall();
    perror(x);
    exit(EXIT_FAILURE);
}

/**
 * Bemerkung: Das Ergebnis ist nicht das erwartete, weil counter++ keine elementare Operation ist und Race-Conditions enstehen. Wie diese genau aussehen
 * hängt vom jeweiligen Scheduling ab. 
 */


int main(void){
    int children = 1000;
    int additions = 10000;

    int shmid;
    int *counter;

    //get shared memory, if it doesnt exist, create it
    if( (shmid = shmget(SHM_KEY,SHM_SIZE,IPC_CREAT|0666)) < 0 )
        err_sys("shmget");

    //attach shared memory
    if( (counter = shmat(shmid,NULL,0)) < 0)
        err_sys("shmat");

    
    //do additions without protection
    for(int i = 0;i<children;i++){
        switch(fork()){
            case -1:
                err_sys("fork");
                break;
            case 0:
                for(int j=0;j<additions;j++){
                    (*counter)++;
                }
                exit(EXIT_SUCCESS);
        }
    }

    //wait for all children
    while( wait(NULL) > 0);
    
    //print result when all children have finished
    fprintf(stdout,"Result from sender: %d\n",*counter);

    //write result to pipe
    FILE* fifo_fp;
    if( (fifo_fp = fopen(FIFO,"w")) == NULL )
        err_sys("fopen w");

    fprintf(fifo_fp,"%d",*counter);

    //close fifo
    if( fclose(fifo_fp) < 0 )
        err_sys("fclose");
    
    //detach shared memory
    if( shmdt(counter) < 0 )
        err_sys("shmdt");

    //delete shared memory id 
    if( shmctl(shmid,IPC_RMID,0) < 0 )
        err_sys("shmctl");
    
    return EXIT_SUCCESS;

}